
# Importieren der benötigten Pakete
import os
import pandas as pd
import numpy as np
import statsmodels.api as sm
import matplotlib.pyplot as plt
 
# Pfad zum Ordner
ordnerpfad = "./converted_xlsx" # \ -> /
dateiname100 = "240325_M100_individual_throughput 1.xlsx" # in Form: "houseprice.xlsx"
dateiname110 = "240325_M110_individual_throughput 1.xlsx"
dateiname120 = "240325_M120_individual_throughput 1.xlsx"
mappenname100 = "240325_M100_individual_throughp"
mappenname110 = "240325_M110_individual_throughp"
mappenname120 = "240325_M120_individual_throughp"


os.chdir(ordnerpfad)

data100 = pd.read_excel(dateiname100,mappenname100)
data110 = pd.read_excel(dateiname110,mappenname110)
data120 = pd.read_excel(dateiname120,mappenname120)


#get the format of the tıme and date
#print(data100['time'][0])
#print(data100['time'][1])
all = []
def calc_throughput(dataframe, name_of_picture):
    i=0
    j=0
    sum=0
    throughput = []
    #get index for one hour
    while dataframe['time'][i] != "2024-03-25 01:00:00.000":
        i +=1
        #print(i)

    for x in range(0,24):
        #get the total throughput
        while j < i:
            sum += dataframe['Summe'][j]
            j+=1

        #print("Total throughput per hour", sum)

        throughput.append(sum)
        j=i
        i=i+720
        sum=0

    time_axes = np.arange(1,25)
    plt.figure()
    plt.title('Throughput per Hour', fontsize=16, color='blue')
    plt.ylabel('Material [Flaeche]')
    plt.xlabel('Stunden')
    plt.bar(time_axes, np.array(throughput))
    plt.savefig(name_of_picture)
    all.append(np.array(throughput))



calc_throughput(data100, "./throughput100.png")
calc_throughput(data110, "./throughput110.png")
calc_throughput(data120, "./throughput120.png")
print(all)
#diff = all[0] - all[1]
#print(diff)

#sum up the rows (check with excel)
"""
data100_new = data100.drop(columns=["Summe"])
data100_new['SummePandas'] = data100_new.sum(axis=1, numeric_only=True)
print(data100_new.head(5))
"""
# Überblick über die Daten
# print(data.head())
# print(data.info())
# print(data.describe())