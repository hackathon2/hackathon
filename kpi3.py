
import os
import pandas as pd
import numpy as np
import statsmodels.api as sm
import matplotlib.pyplot as plt

dateiname100 = "./csv/240325_M100_individual_throughput 1.csv" # in Form: "houseprice.xlsx"
dateiname110 = "./csv/240325_M110_individual_throughput 1.csv"
dateiname120 = "./csv/240325_M120_individual_throughput 1.csv"

data100 = pd.read_csv(dateiname100)
data110 = pd.read_csv(dateiname110)
data120 = pd.read_csv(dateiname120)

TM = data100['PET-Flasche'].sum()
f = data120['PET-Flasche'].sum()

recovery = (TM - f) / TM
print(recovery)