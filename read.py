
# Importieren der benötigten Pakete
import os
import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt
 
# Pfad zum Ordner
ordnerpfad = "/home/chris/Dokumente/Hackator/hackathon/converted_xlsx" # \ -> /
dateiname100 = "240325_M100_individual_throughput 1.xlsx" # in Form: "houseprice.xlsx"
dateiname110 = "240325_M110_individual_throughput 1.xlsx"
dateiname120 = "240325_M120_individual_throughput 1.xlsx"
mappenname100 = "240325_M100_individual_throughp"
mappenname110 = "240325_M110_individual_throughp"
mappenname120 = "240325_M120_individual_throughp"


os.chdir(ordnerpfad)

data100 = pd.read_excel(dateiname100,mappenname100)
data110 = pd.read_excel(dateiname110,mappenname110)
data120 = pd.read_excel(dateiname120,mappenname120)


interval = 1000
offset = 5

def get_interval_data(data):
    i_res = []
    for i in range(0, len(data), interval):
        sum = 0
        tstamp = data['time'][i]
        for j in range(interval):
            sum += data['Summe'][i]
        i_res.append((tstamp, sum))
    return i_res

print(get_interval_data(data100))

# Überblick über die Daten
# print(data.head())
# print(data.info())
# print(data.describe())

